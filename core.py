import json
import logging
import re
import requests
import time
from settings import (proxies,
                      tenant_id,
                      client_id,
                      scope,
                      client_secret,
                      grant_type,
                      )
from urllib.parse import urljoin


base_url = "https://graph.microsoft.com/v1.0"
authorized_headers = {}
b = requests.Session()
if proxies:
    b.proxies.update(proxies)


def get_token():
    token_url = f"https://login.microsoftonline.com/{tenant_id}/oauth2/v2.0/token"
    data = {'client_id': client_id, 'scope': scope, 'client_secret': client_secret, 'grant_type': 'client_credentials'}
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    r = b.post(token_url, data=data, headers=headers)
    return r.json()


def update_token():
    token = get_token()
    authorized_headers.update(
        {'Authorization': f'Bearer {token["access_token"]}',
        'Content-Type': 'application/json'}
        )


def call_api(url, method='get', data=None):
    """Calls the MS Graph api, handling pagination and token renewal

    Calls Microsoft Graph API.  If multiple pages are available,
    all of them will be retreived.  If the auth token is out of date, a new
    one is created on the fly.

    Arguments:
        url {string} -- url of the graph endpoint
    
    Keyword Arguments:
        method {str} -- http method to use when calling the api (default: {'get'})
        data {dict} -- dict of the request body to be sent (default: {None})

    Raises:
        NotImplementedError -- if a http method that is not implemented is passed
        NameError -- if an invalid http method is passed

    Returns:
        list -- list of items returned from the graph api
    """
    if base_url not in url:
        url = "/".join([base_url, url])
    data = []
    method = method.lower()
    unsupported_http_methods = ["head",
                                "put",
                                "connect",
                                "options",
                                "trace",
                                "patch",
                                ]
    if method == 'get':
        response = b.get(url, headers=authorized_headers)
    elif method == 'post':
        response = b.post(url, headers=authorized_headers, json=data)
    elif method == 'delete':
        response = b.delete(url, headers=authorized_headers)
    elif method in [m for m in unsupported_http_methods]:
        raise NotImplementedError(f"Method '{method}' is not currently supported")
    else:
        raise NameError(f"Not a valid HTTP method: {method}")

    if response.status_code == 204:
        pass  # delete returns an empty response
    elif response.status_code == 429 or response.status_code == 503:
        sleep_time = int(response.headers.get('Retry-After', 300))
        time.sleep(sleep_time)
        data += call_api(url, method=method, data=data)
    elif response.status_code == 401:
        update_token()
        data += call_api(url, method=method, data=data)
    elif response.ok:
        data += response.json().get('value', None)
        next_url = response.json().get("@odata.nextLink", None)
        if next_url:
            data += call_api(next_url, method=method, data=data)

    return data


def get_user_calendar_id(user, calendar):
    """Gets a calendar ID for a given user and calendar name

    Arguments:
        user {string} -- Username including domain (user@domain.com)
        calendar {string} -- Calendar name

    Returns:
        string -- Calendar ID
    """

    assert re.match(r"[^@]+@[^@]+\.[^@]+", user), "user parameter must be an email address"
    calendar_id = None
    endpoint = f"{base_url}/{user}/calendars"
    calendars = call_api(endpoint)
    if calendars:
        calendar_ids = [cal['id'] for cal in calendars if cal['name'] == calendar]
    if calendar_ids:
        calendar_id = calendar_ids[0]

    return calendar_id


def get_term(year=None, term=None):
    """Get a start and end date for a term, defaults to current term
    
    Keyword Arguments:
        year {int} -- Year for the term (default: {None})
        term {int} -- Term number (1-4) (default: {None})

    Returns:
        tuple -- tuple with (start, end) datetime strings
    """

    return ("2018-12-14T01:15:26", "2018-12-24T01:15:26")


def get_events(user, calendar_id, start, end):
    """Get a list of events for a user's calendar

    Arguments:
        user {string} -- user's email address
        start {string} -- isoformat(timespec='microseconds') formatted string
        end {string} -- isoformat(timespec='microseconds') formatted string
        calendar_id {string} -- calendar id hash from microsoft graph
    """

    endpoint = f"{base_url}/{user}/calendars/{calendar_id}/calendarView?startDateTime={start}&endDateTime={end}&$select=subject,bodyPreview,start,end,location"
    return call_api(endpoint)

def delete_event(user, event_id):
    """Delete an event from a user's calendar

    Arguments:
        user {string} -- Username including domain (user@domain.com)
        event_id {string} -- Event ID

    Raises:
        RuntimeError -- Occurs when a non 204 response is received
    """

    endpoint = f"{base_url}/{user}/events/{event_id}"
    response = call_api(endpoint, method='delete')
    if response.status_code != 204:
        raise RuntimeError(f"Delete event should give 204 status code, instead it gave: {response.status_code}")

class ClientFactory:
    def __getattr__(self, name):
        if name in self.__dict__:
            return self.__dict__[name]
        else:
            return call_api("/".join([self._path, name]))

class User(ClientFactory):
    _path = 'users'

    def __init__(self, user=None):
        if user:
            self._path = "/".join([self._path, user])
        else:
            self._path = 'me'

class Calendar:
    def __init__(self, raw_calendar):
        self.id = raw_calendar['id']
        self.canEdit = raw_calendar.get('canEdit', False)
        self.canShare = raw_calendar.get('canShare', False)
        self.canViewPrivateItems = raw_calendar.get('canViewPrivateItems', False)
        self.color = raw_calendar.get('color', 'auto')
        self.owner = Owner(raw_calendar.get('owner'))
        self.name = raw_calendar.get('name')

class Owner:
    def __init__(self, raw_owner):
        self.email = raw_owner.get('address')
        self.name = raw_owner.get('name')
    
    def __repr__(self):
        return f"{self.email} <{self.name}>"
import unittest
from graph import get_user_calendar_id

class TestGetUserCalendarId(unittest.TestCase):

    def test_enforce_email_username(self):
        with self.assertRaises(AssertionError):
            get_user_calendar_id("abc", "Calendar")
    

if __name__ == '__main__':
    unittest.main()